import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";

const Products = () => {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('http://localhost:5000/products')
            .then(res => res.json())
            .then(data => setProducts(data));
    }, []);

    // Delete Product Handler
    const deleteProductHandler = id => {
        const proceed = window.confirm("Are you sure, you want to delete?");
        if (proceed) {
            const url = `http://localhost:5000/products/${id}`;
            fetch(url, {
                method: 'DELETE'
            })
                .then(res => res.json())
                .then(data => {
                    if (data.deletedCount > 0) {
                        alert("Deleted Successfully.");
                        const remainingProducts = products.filter(product => product._id !== id);
                        setProducts(remainingProducts);
                    }
                })
        }
    };

    return (
        <div>
            <h2>Total Products: {products.length}</h2>
            <ul>
                {
                    products.map(product => <li
                        key={product._id}
                    >
                        Name: {product.name} => Price: {product.price} => Quantity: {product.quantity}
                        <Link to={`/products/update/${product._id}`}><button>Update</button></Link>
                        <button onClick={() => deleteProductHandler(product._id)}>X</button>
                    </li>)
                }
            </ul>
        </div>
    );
};

export default Products;
