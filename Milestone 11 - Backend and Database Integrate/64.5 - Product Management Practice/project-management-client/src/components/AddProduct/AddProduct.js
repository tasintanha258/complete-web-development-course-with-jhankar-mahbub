import React, {useRef} from 'react';

const AddProduct = () => {
    const nameRef = useRef();
    const priceRef = useRef();
    const quantityRef = useRef();

    const handleAddProduct = e => {
        const name = nameRef.current.value;
        const price = priceRef.current.value;
        const quantity = quantityRef.current.value;

        const newProduct = {name: name, price: price, quantity: quantity};

        const url = 'http://localhost:5000/products';
        fetch(url, {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(newProduct)
        })
            .then(res => res.json())
            .then(data => {
                if (data.insertedId) {
                    alert(name + " Succeessfully Added");
                    e.target.reset();
                }
            })
        e.preventDefault();
    };

    return (
        <div>
            <h2>Please Add User</h2>
            <form onSubmit={handleAddProduct}>
                <input type="text" ref={nameRef} placeholder="Product Name"/>
                <input type="number" ref={priceRef} placeholder="Product Price"/>
                <input type="number" ref={quantityRef} placeholder="Product Quantity"/>
                <input type="submit" value="Add"/>
            </form>
        </div>
    );
};

export default AddProduct;
