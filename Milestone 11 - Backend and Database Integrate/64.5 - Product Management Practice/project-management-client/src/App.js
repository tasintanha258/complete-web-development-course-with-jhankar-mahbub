import './App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import AddProduct from "./components/AddProduct/AddProduct";
import Products from "./components/Products/Products";
import UpdateProducts from "./components/UpdateProducts/UpdateProducts";

function App() {
    return (
        <div className="App">
            <Router>
                <Header/>
                <Switch>
                    <Route exact path="/">
                        <Home/>
                    </Route>
                    <Route exact path="/products">
                        <Products/>
                    </Route>
                    <Route path="/products/add">
                        <AddProduct/>
                    </Route>
                    <Route path="/products/update/:id">
                        <UpdateProducts/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
