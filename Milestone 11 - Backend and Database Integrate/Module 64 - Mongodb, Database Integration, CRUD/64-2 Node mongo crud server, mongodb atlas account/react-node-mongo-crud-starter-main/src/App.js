import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import './App.css';
import AddUser from "./components/AddUser/AddUser";
import Home from "./components/Home/Home";
import UpdateUser from "./components/UpdateUser/UpdateUser";
import Users from "./components/Users/Users";
import Header from './components/Header/Header';

function App() {
    return (
        <div className="App">
            <Router>
                <div>
                    <Header/>
                    <Switch>
                        <Route exact path="/">
                            <Home/>
                        </Route>
                        <Route exact path="/users">
                            <Users/>
                        </Route>
                        <Route path="/users/add">
                            <AddUser/>
                        </Route>
                        <Route path="/users/update/:id">
                            <UpdateUser/>
                        </Route>

                    </Switch>
                </div>
            </Router>
        </div>
    );
}

export default App;
