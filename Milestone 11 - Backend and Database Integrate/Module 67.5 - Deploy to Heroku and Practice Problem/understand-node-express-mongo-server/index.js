// >>67-1 Module Introduction, Understanding Node, build in methods<<

// const os = require('os');
// const fs = require('fs');
//
// console.log("OS Version: ", os.version());
// console.log("Platform: ", os.platform());
// console.log("Total Memory: ", os.totalmem());
// console.log("Architecture: ", os.arch());
//
// // fs.writeFileSync('hello.txt', "I am Writing from Node");
// // fs.appendFileSync('hello.txt', "\nNew Line Text.");
//
// const content = fs.readFileSync('hello.txt');
// console.log(content.toString());

// >>67-2 Simple express server, Explore request and response<<

const express = require('express');
const app = express();
const port = 7000;

app.get('/', (req, res) => {
    res.send("Hello World from Express Server!");
});

app.listen(port, () => {
    console.log("Listening to Port: ", port);
});