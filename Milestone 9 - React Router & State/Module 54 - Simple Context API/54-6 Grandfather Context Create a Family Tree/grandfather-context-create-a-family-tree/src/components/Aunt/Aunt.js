import React, {useContext} from 'react';
import {RingContext} from "../../App";

const Aunt = () => {
    const [ornaments, house] = useContext(RingContext);

    return (
        <div>
            <h2>Aunt</h2>
            <small>House: {house}</small>
            <p>{ornaments}</p>
            <br/>
            <p>Context House: {house}</p>
        </div>
    );
};

export default Aunt;
