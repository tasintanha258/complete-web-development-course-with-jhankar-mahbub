import React from 'react';
import Father from "../Father/Father";
import Uncle from "../Uncle/Uncle";
import Aunt from "../Aunt/Aunt";

const Grandfather = (props) => {
    const {house} = props;
    return (
        <div>
            <h2>Grandfather</h2>
            <h5>House: {house}</h5>
            <div style={{display: 'flex'}}>
                <Father house={house}/>
                <Uncle house={house}/>
                <Aunt house={house}/>
            </div>
        </div>
    );
};

export default Grandfather;
