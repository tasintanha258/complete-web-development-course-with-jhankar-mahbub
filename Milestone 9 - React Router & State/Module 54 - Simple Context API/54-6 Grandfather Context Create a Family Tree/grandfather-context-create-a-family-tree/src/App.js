import logo from './logo.svg';
import './App.css';
import Grandfather from "./components/Grandfather/Grandfather";
import {createContext, useState} from "react";

export const RingContext = createContext("Ring");

function App() {
    const [house, setHouse] = useState(2);
    const ornaments = "Ruby Ring";
    return (
        <RingContext.Provider value={[ornaments, house]}>
            <div className="App">
                <button
                    onClick={() => setHouse(house + 1)}
                >Buy a new House
                </button>

                <Grandfather house={house} ornaments={ornaments}/>
            </div>
        </RingContext.Provider>
    );
}

export default App;
