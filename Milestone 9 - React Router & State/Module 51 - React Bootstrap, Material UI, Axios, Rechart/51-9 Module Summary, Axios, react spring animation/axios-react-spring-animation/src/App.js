import logo from './logo.svg';
import './App.css';
import {useSpring, animated} from 'react-spring';
import {useState} from "react";

function App() {
    const [flip, setFlip] = useState(false);

    return (
        <div className="App">
            <animated.div style={useSpring({
                to: {opacity: 1},
                reset: true,
                reverse: flip,
                delay: 200,
                onRest: () => setFlip(!flip),
                from: {opacity: 0}
            })}>
                This is Spring Animation!
            </animated.div>
        </div>
    );
}

export default App;
