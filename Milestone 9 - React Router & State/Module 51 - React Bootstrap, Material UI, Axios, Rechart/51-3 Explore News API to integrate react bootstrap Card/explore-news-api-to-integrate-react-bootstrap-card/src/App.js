import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {useEffect, useState} from "react";
import {Card, Col, Row, Spinner} from "react-bootstrap";
import News from "./components/News/News";

function App() {
    const [news, setNews] = useState([]);
    useEffect(() => {
        fetch('https://newsapi.org/v2/everything?q=tesla&from=2022-01-24&sortBy=publishedAt&apiKey=f2bf623c2b0c4085b8e5f87eb936fece')
            .then(res => res.json())
            .then(data => setNews(data.articles));
    }, []);

    return (
        <div className="App">
            {
                news.length === 0 ? <Spinner animation="border" variant="success" /> :
                    <Row xs={1} md={4} className="g-4">
                        {
                            news.map(nws => <News news={nws}></News>)
                        }
                    </Row>
            }
        </div>
    );
}

export default App;
