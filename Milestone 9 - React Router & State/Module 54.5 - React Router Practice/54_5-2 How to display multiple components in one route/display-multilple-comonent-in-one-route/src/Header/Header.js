import React from 'react';
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <div>
            <h2>This is Header</h2>
            <Link to="/home">Home</Link>
            <Link to="/about">About</Link>
            <Link to="/contact">Contact</Link>
            <Link to="/more">More</Link>
        </div>
    );
};

export default Header;
