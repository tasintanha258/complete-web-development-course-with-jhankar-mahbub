import React, {useEffect, useState} from 'react';
import useTodos from "../../hooks/useTodos";
import Todos from "../Todos/Todos";

const Completed = () => {
    const [todos, setTodos] = useTodos();
    const [allComplete, setAllComplete] = useState([]);

    useEffect(() => {
        const completeList = todos.filter(todo => todo.completed === true);
        setAllComplete(completeList);
    }, [todos]);

    return (
        <div>
            <h2>This is Completed List</h2>
            <h3>{allComplete.length}, tasks completed.</h3>
            {
                allComplete.length > 0 &&
                allComplete.map(todo => <Todos
                    key={todo.id}
                    todo={todo}
                >
                </Todos>)
            }
        </div>
    );
};

export default Completed;
