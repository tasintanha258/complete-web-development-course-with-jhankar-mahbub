import React from 'react';
import './header.css';
import logo from '../../images/img.png';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <div className="header">
            <img className="logo" src={logo} alt=""/>
            <nav>
                <NavLink to="/home">Home</NavLink>
                <NavLink to="/completed">Completed</NavLink>
                <NavLink to="/notcompleted">Not Completed</NavLink>
            </nav>
        </div>
    );
};

export default Header;
