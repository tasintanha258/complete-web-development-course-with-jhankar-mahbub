import React from 'react';
import {MDBContainer, MDBFooter} from "mdb-react-ui-kit";

const Footer = () => {
    return (
        <MDBFooter color="blue" className="font-small pt-4 mt-4">
            <div className="footer-copyright text-center py-3">
                <MDBContainer fluid>
                    &copy; {new Date().getFullYear()} Copyright: Ashraful Islam
                </MDBContainer>
            </div>
        </MDBFooter>
    );
};

export default Footer;
