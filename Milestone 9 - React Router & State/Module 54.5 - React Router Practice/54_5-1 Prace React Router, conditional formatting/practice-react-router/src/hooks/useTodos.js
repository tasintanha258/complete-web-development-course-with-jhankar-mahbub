import {useEffect, useState} from "react";

const useTodos = () => {
    const [todos, setTodos] = useState([]);

    useEffect(() => {
        fetch('./todos.json')

            .then(res => res.json())
            .then(data => {
                console.log(data);
                setTodos(data);
            });
    }, []);
    return [todos, setTodos];
};

export default useTodos;