let first = 2;
let second = 2;

if (first == second) {
    console.log("condition is true");
} else {
    console.log("Condition is false");
}

first = 2;
second = '2';

if (first == second) {
    console.log("condition is true");
} else {
    console.log("Condition is false");
}

first = 2;
second = '2';

if (first === second) {
    console.log("condition is true");
} else {
    console.log("Condition is false");
}

const a = {name: "John"}
const b = {name: "John"}

if (a == b) {
    console.log('both are same');
} else {
    console.log('both are not same');
}


const x = [];
const y = [];

if (x == y) {
    console.log('both are same');
} else {
    console.log('both are not same');
}