const numbers = [5, 1, 28, 36, 5, 8, 96];
console.log(numbers);
console.log(...numbers);

const max = Math.max(23, 45, 99, 5);
console.log(max);
const maxInArray = Math.max(...numbers);
console.log(maxInArray);

const number2 = [23, ...numbers, 900];
numbers.push(555);
console.log(numbers);
console.log(number2);