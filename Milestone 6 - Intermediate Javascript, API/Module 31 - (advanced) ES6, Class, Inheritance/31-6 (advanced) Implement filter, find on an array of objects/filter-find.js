const numbers = [15, 8, 17, 23, 96, 45, 86, 35, 74];
const bigNumbers = numbers.filter(number => number > 20);
const smallNumbers = numbers.filter(number => number < 20);
console.log(bigNumbers);
console.log(smallNumbers);

const products = [
    {name: "Water-Bottle", price: 80, color: "yellow"},
    {name: "Mobile", price: 50000, color: "black"},
    {name: "Laptop", price: 180000, color: "Ash"},
    {name: "Smart-Watch", price: 4500, color: "orange"},
    {name: "Speaker", price: 2000, color: "Black"},
]

const expensive = products.filter(product => product.price >= 50000);
console.log(expensive);

const blacks = products.filter(product => product.color.toLowerCase() === "black");
console.log(blacks);

const ashItem = products.find(product => product.color === "Ash");
console.log(ashItem);