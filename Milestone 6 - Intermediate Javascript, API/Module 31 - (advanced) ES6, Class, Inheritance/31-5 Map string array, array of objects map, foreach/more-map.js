const friends = ["Tom Hanks", "Tom Cruise", "Tom Brady", "Ashraful Islam"];
const fLengths = friends.map(friends => friends.length);
console.log(fLengths);

const products = [
    {name: "Water-Bottle", price: 80, color: "yellow"},
    {name: "Mobile", price: 50000, color: "black"},
    {name: "Laptop", price: 180000, color: "Ash"},
    {name: "Smart-Watch", price: 4500, color: "orange"},
    {name: "Speaker", price: 2000, color: "Black"},
]

const productNames = products.map(product => product.name);
console.log(productNames);

products.map(product => console.log(product));
products.forEach(product => console.log(product));
