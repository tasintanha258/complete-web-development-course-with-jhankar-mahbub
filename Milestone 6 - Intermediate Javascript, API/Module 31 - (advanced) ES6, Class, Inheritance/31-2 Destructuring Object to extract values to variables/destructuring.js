const fish = {
    id: 45, name: "Hilsha", address: "Padma River", price: 2000, phone: 456987123, dress: "Silver"
};

const {name, price, address} = fish;
console.log(price);
console.log(price, address);
console.log(name, address);

const company = {
    name: "Galaxy Tech",
    ceo: {id: 1, name: "Ashraful Islam", responsibility: "Maintain Company"},
    web: {
        work: "Website Development",
        employee: 17,
        framework: "React",
        tech: {first: "html", second: "css", third: "js"}
    },
    Income: 100000
}

// const work = company.web.work;
// const framework = company.web.framework;

const {work, framework} = company.web;
const {responsibility} = company.ceo;
const {second, third} = company.web.tech;
console.log(work, framework, responsibility, second, third);
