class TeamMember {
    name;
    address = "Bangladesh";

    constructor(name, address) {
        this.name = name;
        this.address = address;
    }
}

class Support extends TeamMember {
    groupSupportTime;
    designation = "Support Web Dev";
    constructor(name, address, designation, time) {
        super(name, address);
        this.designation = designation;
        this.groupSupportTime = time;
    }

    startSession() {
        console.log(this.name, "Start a support session");
    }
}

class StudentCare extends TeamMember{
    groupSupportTime;
    designation = "Student Care";
    constructor(name, address, designation, time) {
        super(name, address);
        this.groupSupportTime = time;
        this.designation = designation;
    }

    buildARoutine(student) {
        console.log(this.name, "Build a routine for, ", student);
    }
}

class NeptuneDev extends TeamMember {
    codeEditor;
    designation = "Neptune Dev App";
    constructor(name, address, designation, editor) {
        super(name, address);
        this.designation = designation;
        this.codeEditor = editor;
    }

    releaseApp(version) {
        console.log(this.name, "release app version, ", version);
    }
}

const ashraful = new Support("Ashraful Islam", "Chittagong",5);
const akhi = new Support("Akhi", "Dhaka",6);
console.log(ashraful);
console.log(akhi);
ashraful.startSession();

const rifat = new StudentCare("Rifat", "Dubai");
console.log(rifat);

const aziz = new NeptuneDev("Aziz", "Dhaka", "Webstorm");
console.log(aziz);
aziz.releaseApp(1.254);