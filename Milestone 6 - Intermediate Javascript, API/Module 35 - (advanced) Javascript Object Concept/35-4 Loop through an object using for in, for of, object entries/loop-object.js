const bottle = {
    color: 'Blue',
    holds: 'Water',
    price: 50,
    isClean: true,
}

for (let prop in bottle) {
    console.log(prop, ',', bottle[prop]);
}

const keys = Object.keys(bottle);
console.log(keys);
for (const prop of keys) {
    console.log(prop, ',', bottle[prop]);
}

// Advanced
console.log("<---------Advanced---------->");
for (const [key, value] of Object.entries(bottle)) {
    console.log(key, value);
}