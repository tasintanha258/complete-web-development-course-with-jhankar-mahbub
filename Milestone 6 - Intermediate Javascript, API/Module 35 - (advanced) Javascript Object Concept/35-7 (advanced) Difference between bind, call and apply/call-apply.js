const ashraful = {
    id: 102,
    money: 5000,
    name: "Ashraful Islam",
    expenditure: function (expense, vat) {
        this.money = this.money - expense - vat;
        console.log(this);
        return this.money;
    },
};

const akhi = {
    id: 103,
    money: 7000,
    name: "Akhi",
};
// Using call; comma seperated parameter
/*
ashraful.expenditure.call(ashraful, 500, 50);
akhi.expenditure.call(akhi, 600, 90);*/

// Using apply, Array parameter
ashraful.expenditure.apply(akhi, [400, 50]);
