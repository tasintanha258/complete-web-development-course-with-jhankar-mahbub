const first = {a: 1}
const second = {a: 1}

if (first === second) {
    console.log('Objects are equal');
} else {
    console.log('Objects are not equal');
}

const third = {a: 1}
const fourth = {a: 1}
const fifth = third;

if (third === fifth) {
    console.log('Objects are equal');
} else {
    console.log('Objects are not equal');
}

