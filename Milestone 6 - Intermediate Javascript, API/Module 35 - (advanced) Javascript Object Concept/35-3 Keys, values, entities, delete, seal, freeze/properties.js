const bottle ={
    color: 'Blue',
    holds: 'Water',
    price: 50,
    isClean: true,
}
const keys = Object.keys(bottle);
console.log(keys);
const value = Object.values(bottle);
console.log(value);
const pairs = Object.entries(bottle);
console.log(pairs);

// Object.seal(bottle); // Doesn't allow deletion
Object.freeze(bottle); // Doesn't allow modification
bottle.price = 200;
bottle.height = 14;
delete bottle.isClean;
console.log(bottle);