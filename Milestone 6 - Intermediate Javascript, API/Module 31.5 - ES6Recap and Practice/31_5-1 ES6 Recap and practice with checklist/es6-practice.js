let name = "Ashraful Islam";
name = "Akhi";
console.log(name);

const number = 9;
// number = 8; // Assignment to constant variable not possible
console.log(number);

// backticked line
const song = `I did it my way`;
const multiLine = `Lorem ipsum dolor sit amet,
 consectetur adipisicing 
 elit. Cupiditate, minima.`;
console.log(multiLine);

// Dynamically variable use inside backticked line
const friendCount = 5;
const oldHeader = `<h3 class="friend-name">Friend-${friendCount}</h3>`;
console.log(oldHeader);

const firstName = "Ashraful";
const lastName = "Islam";
const fullName = `My full name is: ${firstName} ${lastName}`;
console.log(fullName);

// Arrow function
let divByFive = (num1) => num1 / 5;
console.log(divByFive(15));

const result = (num1, num2) => (num1 + 2) * (num2 + 2);
console.log(result(2, 2));

const multiply = (num1, num2, num3) => num1 * num2 * num3;
console.log(multiply(2, 3, 4));

const result2 = (x, y) => {
    return x * y;
}
console.log(result2(2, 3));

//  Using map
const numbers = [2, 5, 7, 8, 9, 6, 3];
const multiple = numbers.map(numbers => numbers * 5);
console.log(multiple);

// Using filter
const oddNumbers = numbers.filter(numbers => (numbers % 2 !== 0));
console.log(oddNumbers);


// Using find
const products = [
    {name: "Water-Bottle", price: 80, color: "yellow"},
    {name: "Mobile", price: 50000, color: "black"},
    {name: "Laptop", price: 180000, color: "Ash"},
    {name: "Smart-Watch", price: 4500, color: "orange"},
    {name: "Speaker", price: 5000, color: "Black"},
]

const fiveKProduct = products.find(product => product.price === 5000);
console.log(fiveKProduct);

// Destructuring
const [first, second, third] = [2, 5, 6, 9, 8, 7];
console.log(third);

// Function with default value parameter
function myFunction(num1, num2, num3 = 9) {
    return num1 + num2 + num3;
}

console.log(myFunction(1, 2, 5));