const numbers = [42, 85, 25];

const [x, y] = [15, 9];
console.log(x, y);

const [p, q, r] = numbers;
console.log(p, q, r);

const student = {
    name: "Ashraful Islam",
    id: 482266,
    subjects: ['Algorithm', 'Data Structure', 'Digital Logic Design'],
};

const [firstSubject, secondSubject] = student.subjects;


// Object destructuring
const {name, id} = {name: 'Ashraful islam', id: 45157};
const {name, id} = {age: 24, name: 'Ashraful islam', id: 45157};

const employee = {
    name: "Ashraful Islam",
    id: 482266,
    machine: 'Windows 10',
    language: ['Javascript', 'HTML', 'Java'],
    specifications: {
        ide: {
            name: 'Webstorm',
            company: 'Jetbrains',
            price: 120,
        },
        height: 5.7,
        weight: 58,
        favFood: 'Biriani',
    }
};

const {machine, language} = employee;
const {weight, favFood} = employee.specifications;
const {company} = employee?.specifications?.ide; // Optional chaining-- (?.ide)