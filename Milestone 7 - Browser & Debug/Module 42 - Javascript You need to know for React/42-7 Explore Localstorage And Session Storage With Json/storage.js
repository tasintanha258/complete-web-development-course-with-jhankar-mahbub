const addToLocalStorage = () => {
    /**
     *
     * @type {HTMLInputElement}
     */
    const idIdInput = document.getElementById('storage-id');
    const id = idIdInput.value;

    const valueInput = document.getElementById('storage-value');
    const value = valueInput.value;

    if (id && value) {
        localStorage.setItem(id, value);
    }
    idIdInput.value = '';
    valueInput.value = '';

    localStorage.setItem('friends', JSON.stringify([1, 54, 879, 63, 556]));

    const pen ={
        price: 5,
        color: 'black',
        brand: 'Red Leaf'
    }

    localStorage.setItem('Pen', JSON.stringify(pen));
}