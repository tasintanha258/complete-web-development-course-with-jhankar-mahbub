let num = 9;
if (num) {
    num *= 90;
} else {
    num = 0;
}

let money = 900;
let food;
if (money > 500) {
    food = 'Biriyani';
} else {
    food = 'Bread and Tea';
}

//Shortcut / ternary
let food2 = money > 500 ? 'Biriyani' : 'Bread or Tea';
console.log(food2);

// number to string
const num1 = 80;
console.log(num1);
const numString = num1 + '';
console.log(numString);

//string to number
const input = '809';
console.log(input);
const inputNum = +input;
console.log(inputNum);

let isActive = true;
const showUser = () => console.log('Display User');
const hideUser = () => console.log('Hide User');
isActive ? showUser() : hideUser();
isActive && showUser();
isActive || showUser();

