const products = [
    {name: "Laptop", price: 2000, brand: "Asus", color: "Black"},
    {name: "phone", price: 1000, brand: "Samsung", color: "Blue"},
    {name: "Camera", price: 5000, brand: "Canon", color: "black"},
    {name: "Watch", price: 1200, brand: "Rayban", color: "Navy"},
];

const brands = products.map(product => product.brand);
console.log(brands);

const prices = products.map(product => product.price);
console.log(prices);

products.forEach(product => console.log(product.color));

const cheap = products.filter(product => product.price <= 2000);
console.log(cheap);

const specificName = products.filter(product => product.name.includes('n'));
console.log(specificName);

const special = products.find(product => product.name.includes('n'));
console.log(special);