/*
function add(number1: number, number2: number): number {
    return number1 + number2;
}

const output: number = add(4, 6);
console.log('Sum = ', output);*/

/*
//multi purpose
function add(first: number | string, second: number| string): number| string {
    const result: number | string = first + second;
    return result;
}

const fullName: number| string = add('Ashraful', ' Islam');
console.log(fullName);
*/

function doubleConsole(number: number):void {
    console.log(2 * number);
}

