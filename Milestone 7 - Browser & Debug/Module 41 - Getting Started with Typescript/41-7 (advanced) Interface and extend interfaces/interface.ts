interface Player {
    name: string,
    club: string,
    salary: number,
    wife?:string,
    previousClubs?: string[];
}

const Sakib: Player = {
    name: 'Sakib-Al-Hasan',
    club: 'Dhaka Dynamytes',
    salary: 300000,
    previousClubs: ['Moon Club', 'Mars Club']
};

const Ashraful: Player = {
    name: 'Ashraful Islam',
    club: 'The Universe',
    salary: 9000000,
    wife: 'Akhi'
};

interface Employee {
    name: string;
    designation: string;
    salary: number;
    age: number;
}

interface Developer extends Employee{
    language: string;
    codeEditor: string;
    typingSpeed: number;
}

const ashraful: Developer = {
    name: 'Ashraful Islam',
    designation: 'Front-End Developer',
    salary: 200000,
    age: 26,
    language: 'Javascript',
    codeEditor: 'Webstorm',
    typingSpeed: 55
};