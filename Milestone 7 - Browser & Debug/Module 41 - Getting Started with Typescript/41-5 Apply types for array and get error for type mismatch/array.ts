const salary: number = 9800;
const friendSalary: number[] = [9800, 4560, 13000, 15000, 20000];
const friends: string[] = ['Ashraful', 'Akhi', 'Shukhy', 'Naeem'];
friendSalary[0] = 19000;
friendSalary.push(22000);
console.log(friendSalary);

friends[0] = 'Sakib';
friends.push('Akib');
console.log(friends);

let max: number = 0;
for (const salary of friendSalary) {
    if (salary > max) {
        max = salary;
    }
}
console.log(max);


