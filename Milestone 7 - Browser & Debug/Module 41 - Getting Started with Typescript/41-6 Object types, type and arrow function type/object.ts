const city: string = 'Dhaka';

type Person = {
    name: string;
    age: number;
    salary: number;
}
const student: Person = {
    name: 'Ashraful Islam',
    age: 89,
    salary: 45000
};

const employee: { name: string, age: number, salary: number } = {
    name: 'Akhi',
    age: 24,
    salary: 90000,
};