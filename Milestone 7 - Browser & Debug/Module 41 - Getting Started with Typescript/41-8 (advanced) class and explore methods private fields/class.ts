class Car {
    model: string;
    price: number;
    private _millage: number;

    constructor(model: string, price: number) {
        this.model = model;
        this.price = price;
        this._millage = 100000;
    }

    getActualMillage(): number {
        return this._millage + 50000;
    }

    getTotalPrice(tax: number): number {
        const taxAMount = this.price * (tax / 100);
        return this.price + taxAMount;
    }
}

const tesla = new Car('Model X', 7600000);
const totalPrice: number = tesla.getTotalPrice(10);
console.log(totalPrice);

const millage: number = tesla.getActualMillage();
console.log(millage);
