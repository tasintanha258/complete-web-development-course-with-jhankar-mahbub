import {getAuth, signInWithPopup, GoogleAuthProvider, GithubAuthProvider, FacebookAuthProvider, signOut} from 'firebase/auth';
import './App.css';
import initializeAuthentication from "./Firebase/firebase.initialize";
import {useState} from "react";

initializeAuthentication();

const googleProvider = new GoogleAuthProvider();
const gitHubProvider = new GithubAuthProvider();
const facebookAuthProvider = new FacebookAuthProvider();

function App() {
    const [user, setUser] = useState({});
    const auth = getAuth();

    const handleGoogleSignIn = () => {
        signInWithPopup(auth, googleProvider)
            .then(result => {
                const {displayName, email, photoURL} = result.user;
                const loggedInUser = {
                    name: displayName,
                    email: email,
                    photo: photoURL
                };
                setUser(loggedInUser);
            })
            .catch(error => {
                console.log(error.message);
            });
    };

    const handleGithubSignIn = () => {
        signInWithPopup(auth, gitHubProvider)
            .then(result => {
                const {displayName, email, photoURL} = result.user;
                const loggedInUser = {
                    name: displayName,
                    email: email,
                    photo: photoURL
                };
                setUser(loggedInUser);
            })
    };

    const handleFacebookSignIn = () => {
        signInWithPopup(auth, facebookAuthProvider)
            .then(result => {
                const {displayName, email, photoURL} = result.user;
                console.log(result.user);
                const loggedInUser = {
                    name: displayName,
                    email: email,
                    photo: photoURL
                };
                setUser(loggedInUser);
            })
    };

    const handleSignOut = () => {
        signOut(auth)
            .then(() => {
                setUser({});
            })

    };

    return (
        <div className="App">
            {
                !user.name ?
                <div>
                    <button onClick={handleGoogleSignIn}>Google Sign In</button>
                    <button onClick={handleGithubSignIn}>Github Sign In</button>
                    <button onClick={handleFacebookSignIn}>Facebook Sign In</button>
                </div> :
                <button onClick={handleSignOut}>Sign Out</button>
            }
            <br/>
            {
                user.name && <div>
                    <h2>Welcome {user.name}</h2>
                    <p>Email: {user.email}</p>
                    <img src={user.photo} alt=""/>
                </div>
            }
        </div>
    );
}

export default App;
