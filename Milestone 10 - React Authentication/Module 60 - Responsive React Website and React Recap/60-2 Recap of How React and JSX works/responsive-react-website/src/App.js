import './App.css';
import Services from "./Pages/Home/Services/Services";
import Home from "./Pages/Home/Home/Home";

function App() {
  return (
    <div className="App">
        <Home/>
    </div>
  );
}

export default App;
