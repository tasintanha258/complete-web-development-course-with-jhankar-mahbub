import React from 'react';
import Expert from "../Expert/Expert";

const experts = [
    {
        id: 1,
        img: 'https://www.floridacareercollege.edu/wp-content/uploads/sites/4/2020/08/12-Reasons-to-Become-an-Automotive-Mechanic-Florida-Career-College.png',
        name: 'Andrew Smith',
        expertize: ' - Engine Expert-'
    },
    {
        id: 2,
        img: 'https://www.mastermechanic.ca/blog/wp-content/uploads/2017/01/mechanic.jpg',
        name: 'John Anderson',
        expertize: '-Polish Expert-'
    },
    {
        id: 3,
        img: 'https://myauctionsheet.com/blog/wp-content/uploads/2020/12/How-Long-Can-a-Dealership-Hold-Your-Car-for-Repair.jpg',
        name: 'Andrew Smith',
        expertize: ' - Engine Expert-'
    },
    {
        id: 4,
        img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQDuXULD0Ac7U3k99gK299eBvJp4Lt8J8Gb-S2UHemygi2IYItRIrITyqq_tYmIxyOfGlY&usqp=CAU',
        name: 'John Anderson',
        expertize: '-Polish Expert-'
    },
    {
        id: 5,
        img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSiCxNA1OdsQ9BRKdCjia-ueoNlLoRUTqhjfOEiZn-RPsLfRsNEMo4CjTrCmcxf2ZRIV5Y&usqp=CAU',
        name: 'Andrew Smith',
        expertize: ' - Engine Expert-'
    },
    {
        id: 6,
        img: 'https://westernfinancialgroup.ca/get/files/image/galleries/Mobile-mechanics-insurance-newsPost.jpg',
        name: 'John Anderson',
        expertize: '-Polish Expert-'
    }
]

const Experts = () => {
    return (
        <div className="container mt-5">
            <h2 className="text-primary">Our Experts</h2>
            <div className="row">
                {
                    experts.map(expert => <Expert
                        key={expert.id}
                        expert={expert}
                    />)
                }
            </div>
        </div>
    );
};

export default Experts;
