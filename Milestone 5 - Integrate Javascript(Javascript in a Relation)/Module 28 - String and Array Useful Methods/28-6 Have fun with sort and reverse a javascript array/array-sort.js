const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const sortedNumbers = numbers.sort();
console.log(sortedNumbers);

const fruits = ['Apple', 'Orange', "Banana", "Strawberry"];
const sortedFruits = fruits.sort();
console.log(sortedFruits);
const reverseFruit = fruits.reverse();
console.log(reverseFruit);

const fruits2 = ["Strawberry", "Apple", "Orange", "Banana", "Lemon"];
const sortedReverse = fruits2.sort().reverse();
console.log(sortedReverse);
// Unicode error solved (Sort solved)
const bigNumbers = [9, 1, 2, 3, 4, 7, 8, 9, 6, 5, 15, 132, 25, 298, 452, 10, 80, 91, 43];
const sortedBigNumbers = bigNumbers.sort(function (a, b) {
    return a - b;
});
console.log(sortedBigNumbers);