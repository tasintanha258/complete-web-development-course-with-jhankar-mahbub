const anthem = "Amar Shonar Bangla Ami Tomay Valobashi";
const splitWord = anthem.split(' ');
console.log(splitWord);
const withoutAWord = anthem.split('a');
console.log(withoutAWord);
// Slice
const specificPortion = anthem.slice(5, 14);
console.log(specificPortion);

// substr
const subStr = anthem.substr(12, 6);
console.log(subStr);

// Substring
const subString = anthem.substring(12, 15);
console.log(subString);

// Concat
const first = "Hello ";
const second = "Javascript";
const finalWord = first.concat(second);
console.log("Concatenated word: ",finalWord);

//Join
const names = ["John", "David", "Steven", "Ashraful", "Hasan"];
// const allJoined = names.join('');
// const allJoined = names.join(' ');
// const allJoined = names.join(',');
// const allJoined = names.join(', ');
const allJoined = names.join('VVV');
console.log(allJoined);