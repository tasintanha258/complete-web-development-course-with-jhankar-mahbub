function megaFruits(fruits) {
    if (Array.isArray(fruits) === false) {
        return "Please provide an array";
    }
    let mega = fruits[0];
    for (const fruit of fruits) {
        if (fruit.length > mega.length) {
            mega = fruit;
        }
    }
    return mega;
}

const fruits = ["Apple", "Orange", "Banana", "Guava", "Pine-Apple", "StrawBerry", "Grape", "Water Melon"];
const megaFruit = megaFruits(fruits);
console.log(megaFruit);
// indexOf
if (fruits.indexOf("Apple") !== -1) {
    console.log("Apple found");
}
//Includes
if (fruits.includes("Orange")) {
    console.log("Orange exists");
}