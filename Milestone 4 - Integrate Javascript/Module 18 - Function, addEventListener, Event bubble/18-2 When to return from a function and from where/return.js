function evenOddChecker(number) {
    if (number % 2 === 0) {
        return (number + " is even.");
    } else {
        return number + " is odd.";
    }
}

function evenOdd(array) {
    let checkedNumber = [];
    for (let i = 0; i < array.length; i++) {
        const number = array[i];
        checkedNumber.push(evenOddChecker(number));
    }
    return checkedNumber;
}

age = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const result1 = evenOdd(age);
console.log(result1);

number = [30, 31, 35, 98, 65, 66, 45];
const result2 = evenOdd(number);
console.log(result2);