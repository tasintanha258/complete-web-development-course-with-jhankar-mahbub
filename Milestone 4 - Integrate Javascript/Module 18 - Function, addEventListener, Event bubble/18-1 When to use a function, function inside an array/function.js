// Checks number is even or, odd
function evenOdd(number) {
    if (number % 2 === 0) {
        console.log(number + " is even number.");
    } else {
        console.log(number + " is odd number.")
    }
}
//Traverse the given array and send individual value to evenOdd function
function arrayTraverser(array) {
    for (let i = 0; i < array.length; i++) {
        let num = array[i];
        evenOdd(num);
    }
}
nums = [5, 12, 45, 7, 96, 32, 45];
arrayTraverser(nums);
console.log("Friends Age: ");
friends_age = [13, 15, 23, 6, 3, 25, 19, 20];
arrayTraverser(friends_age);
