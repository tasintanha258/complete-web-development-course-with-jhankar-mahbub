function explain_callback(name, age, task) {
    console.log('Hello ', name);
    console.log('Your age ', age);
    task();
}

function studyNow() {
    console.log('Preapre for the exam');
}

function playNow() {
    console.log('Go to the playground');
}

function goToUniversity() {
    console.log('Go to the university');
}

explain_callback('Ashraful', 26, studyNow);
explain_callback('Akhi', 25, playNow);
explain_callback('Shukhy', 24, goToUniversity);