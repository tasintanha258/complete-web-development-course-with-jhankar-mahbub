var applePrice = 9;
if (applePrice <= 10) {
    console.log("I will buy apples");
} else {
    console.log("Look for other less expensive fruit");
}

var guess = 6;
if (guess == 6) {
    console.log("You guessed right");
}else if (guess != 6) {
    console.log("You guessed wrong");
} else {
    console.log("You did not give an answer");
}