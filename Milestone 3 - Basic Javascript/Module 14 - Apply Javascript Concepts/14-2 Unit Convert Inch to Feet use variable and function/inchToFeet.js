function inchToFeet(inch) {
    return inch / 12;
}

var result1 = inchToFeet(145);
console.log(result1);

var result2 = inchToFeet(388);
console.log(result2);