function reverse(str) {
    let reverse = "";
    for (let i = 0; i < str.length; i++) {
        let char = str[i];
        reverse = char + reverse;
    }
    return reverse;
}

let sentence = "Hello JS World";
console.log(reverse(sentence));