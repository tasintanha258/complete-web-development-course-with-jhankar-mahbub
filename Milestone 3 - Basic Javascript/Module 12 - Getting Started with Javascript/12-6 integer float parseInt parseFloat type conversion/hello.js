var number1 = 24;
console.log(number1);
var number2 = 45.9;
console.log(number2);
console.log(typeof number2);

console.log(number1 + number2);
var number3 = '45';
var number4 = '56.7';
number3 = parseInt(number3);
number3 = parseFloat(number4);
console.log(number3 + number4);

var number5 = 6;
number5 += '';
console.log(number5);
console.log(typeof number5);

var number6 = 0.3;
var number7 = 0.1;
var total = number6 + number7;
total = total.toFixed(4);
console.log(total);

