var coffeeLine = ['John', 'Rodrigo', 'Harry', 'Reese'];
console.log(coffeeLine);
coffeeLine.unshift('Carter');
console.log(coffeeLine);
coffeeLine.shift();
console.log(coffeeLine);
coffeeLine.unshift('Finch', 'Root');
console.log(coffeeLine);

var partLine = coffeeLine.slice(2, 4);
console.log(partLine);