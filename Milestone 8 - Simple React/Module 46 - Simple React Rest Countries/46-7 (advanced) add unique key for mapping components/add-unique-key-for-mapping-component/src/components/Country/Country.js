import React from 'react';
import './Country.css';

const Country = (props) => {
    const {name, region, capital, population, flag, alpha3Code} = props.country;
    return (
        <div className='country'>
            <h3>This is: {name}</h3>
            <p><small>Code: {alpha3Code}</small></p>
            <h4>Region: {region}</h4>
            <p>Capital: {capital}</p>
            <p>Population: {population}</p>
            <img src={flag} alt=""/>
        </div>
    );
};

export default Country;
