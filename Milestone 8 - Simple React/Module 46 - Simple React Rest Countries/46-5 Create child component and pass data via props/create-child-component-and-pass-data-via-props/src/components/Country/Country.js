import React from 'react';

const Country = (props) => {
    return (
        <div>
            <h3>This is: {props.name}</h3>
            <p>Capital: {props.capital}</p>
            <p>Population: {props.population}</p>
            <img src={props.flag} alt="Flag Image"/>
        </div>
    );
};

export default Country;
