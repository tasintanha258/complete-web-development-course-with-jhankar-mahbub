import React, {useEffect, useState} from "react";
import Country from "../Country/Country";

const Countries = () => {
    const [countries, setCountries] = useState([]);
    useEffect(() => {
        return () => {
            fetch('https://restcountries.com/v2/all')
                .then(res => res.json())
                .then(data => setCountries(data));
        };
    }, []);

    return (
        <div>
            <h1>Hello from Countries: {countries.length}</h1>
            {
                countries.map(country => <Country country={country}>Country Info</Country>)
            }
        </div>
    );
};

export default Countries;
