import logo from './logo.svg';
import './App.css';
import {useEffect, useState} from "react";

function App() {
  return (
      <div className="App">
        <Countries></Countries>
      </div>
  );
}

function Countries() {
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    fetch('https://restcountries.com/v3.1/all')
        .then(res => res.json())
        .then(data => setCountries(data));
  }, []);

  return (
      <div>
        <h1>Traveling around the world!</h1>
        <h3>Countries Available: {countries.length}</h3>
          {
              countries.map(country => <Country name={country.name.common} capital={country.capital}>Country</Country>)
          }
      </div>
  )
}

function Country(props) {
    return (
        <div>
            <h3>Name: {props.name}</h3>
            <p>Capital: {props.capital}</p>
        </div>
    )
}

export default App;
