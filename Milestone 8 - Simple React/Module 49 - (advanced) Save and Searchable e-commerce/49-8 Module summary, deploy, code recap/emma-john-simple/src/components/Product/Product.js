import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import { faShoppingCart} from '@fortawesome/free-solid-svg-icons';
import './Product.css';
import Rating from "react-rating";

const Product = (props) => {
    // console.log(props);
    const {img, name, price, seller, stock, key, star} = props.product;
    const shoppingCartIcon = <FontAwesomeIcon icon={faShoppingCart}/>
    return (
        <div className='product'>
            <div>
                <img src={img} alt=""/>
            </div>
            <div>
                <h4 className='product-name'>{name}</h4>
                <p><small>by: {seller}</small></p>
                <p>Price: {price}</p>
                <p><small>Only {stock} products left</small></p>
                <Rating
                    initialRating={star}
                    emptySymbol="far fa-star icon-color"
                    fullSymbol="fas fa-star icon-color"
                    readonly></Rating>
                <button onClick={() => props.handleAddToCart(props.product)}
                        className='btn-regular'>{shoppingCartIcon} add to cart
                </button>
            </div>
        </div>

    );
};

export default Product;
