import logo from './logo.svg';
import './App.css';
import {useEffect, useState} from "react";

function App() {
    return (
        <div className="App">
            <LoadUsers></LoadUsers>
        </div>
    );
}

function LoadUsers() {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(res => res.json())
            .then(data => setUsers(data));
    }, []);

    return (
        <div>
            <h1>Data Loaded: {users.length}</h1>
            {
                users.map(user => <User name={user.name} phone={user.phone} email={user.email}>Name</User>)
            }
        </div>
    );
}

function User(props) {

    return (
        <div className='user'>
            <h2>Name: {props.name}</h2>
            <p>Phone: {props.phone}</p>
            <p>Phone: {props.email}</p>
        </div>
    )
}

export default App;
