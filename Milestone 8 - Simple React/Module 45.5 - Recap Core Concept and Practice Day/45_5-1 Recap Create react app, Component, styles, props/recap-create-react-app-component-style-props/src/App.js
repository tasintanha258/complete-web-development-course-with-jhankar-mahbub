import logo from './logo.svg';
import './App.css';

function App() {
    return (
        <div className="App">
            <MyComponent brand='Apple' price='90000'>Apple</MyComponent>
            <MyComponent brand='Microsoft' price='10000'>Microsoft</MyComponent>
            <MyComponent brand='Google' price='0'>Google</MyComponent>
            <MyComponent>N/A</MyComponent>
        </div>
    );
}

function MyComponent(props) {
    const myStyle = {
        backgroundColor: 'lightgray',
        color: 'lightsalmon',
        border: '2px solid lightblue',
        borderRadius: '10px',
        margin: '20px',
        padding: '10px'
    };
    return (
        <div style={myStyle}>
            <h1>Inside Component. Brand: {props.brand}</h1>
            <h4>Asking price: {props.price}</h4>
            <p style={{fontWeight: 'bold'}}>Paragraph from inside Component</p>
        </div>
    )
}

export default App;
