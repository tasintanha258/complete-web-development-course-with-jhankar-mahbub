import logo from './logo.svg';
import './App.css';

function App() {
    const number1 = 9;
    const number2 = 6;
    const person = {
        name: 'Ashraful Islam',
        job: 'Software Engineer'
    }

    const person2 = {
        name: 'Akhi',
        job: 'Front-End Web Developer',
    }

    //Applying style inside the tag
    const stylePerson2 = {
        backgroundColor: 'violet',
        color: 'white',
    }
    return (
        <div className="App">
            <header className="App-header">
                <h2>Hello react....</h2>
                <div className="container">
                    <h3>Inside container</h3>
                    <p>Sum of {number1} & {number2} = {number1 + number2}</p>
                    <p style={{color:'skyblue', fontSize:'34px'}}>Name: {person.name}, Job: {person.job}</p>
                    <p style={stylePerson2}>Job: {person2.job}</p>
                    <p>{JSON.stringify(person2)}</p>
                    <p>{JSON.stringify(person)}</p>
                </div>
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    Edit <code>src/App.js</code> and save to reload.
                </p>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
            </header>
        </div>
    );
}

export default App;
