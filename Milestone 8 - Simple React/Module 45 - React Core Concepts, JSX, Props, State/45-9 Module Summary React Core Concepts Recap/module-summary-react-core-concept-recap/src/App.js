import logo from './logo.svg';
import './App.css';
import {useEffect, useState} from "react";

function App() {
    return (
        <div className="App">
            <Counter></Counter>
            <LoadComment></LoadComment>
        </div>
    );
}


// Counter Component
function Counter() {
    const [count, setCount] = useState(0);
    const handleIncrease = () => setCount(count + 1);
    let handleDecrease;
    if (count > 0) {
        handleDecrease = () => setCount(count - 1);
    }
    return (
        <div>
            <h2>Count: {count}</h2>
            <button onClick={handleIncrease}>Increase</button>
            <button onClick={handleDecrease}>Decrease</button>
        </div>
    );
}

// Comment Component
function Comment(props) {
    return (
        <div className='comment'>
            <h4>{props.title}</h4>
            <h5>{props.id}</h5>
            <p>{props.body}</p>
        </div>
    )
}

// Load Comment Component
function LoadComment() {
    const [comments, setComment] = useState([]);
    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(res => res.json())
            .then(data => setComment(data));
    }, []);

    return (
        <div>
            <h3>Load Comments</h3>
            {
                comments.map(comment => <Comment title={comment.title} id={comment.id}
                                                 body={comment.body}>Comment</Comment>)
            }
        </div>
    );
}

export default App;
