import logo from './logo.svg';
import './App.css';

function App() {
    return (
        <div className="App">
            <Person></Person>
            <Person></Person>
            <Person></Person>
            <Friend></Friend>
            <Friend></Friend>
            <Friend></Friend>
        </div>
    );
}

function Person() {
    return (
        <div className="person">
            <h1>Name: Ashraful Islam</h1>
            <h4>Profession: Web-Developer</h4>
        </div>
    );
}

function Friend() {
    return (
        <div style={{
            fontSize: '20px',
            backgroundColor: 'lightgreen',
            color: 'darkgray',
            border: '1px solid blue',
            margin: '20px',
            borderRadius: '10px'
        }}>
            <h2>Name: Aziz Rahman</h2>
            <h5>Profession: Software Engineer</h5>
        </div>
    );
}

export default App;
