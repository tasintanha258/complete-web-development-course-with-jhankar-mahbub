import logo from './logo.svg';
import './App.css';

function App() {
    return (
        <div className="App">
            <Person name = 'Ashraful Islam' profession = 'Web-developer'></Person>
            <Friend name = 'Akhi' profession = 'Front-End Web-Developer'></Friend>

            <Friend2 phone = {'018645456'} address = {'Dhaka'}>Friend-2 Address</Friend2>

        </div>
    );
}
// Components
function Person(props) {
    console.log(props);
    return (
        <div className="person">
            <h1>Name: {props.name}</h1>
            <h4>Profession: {props.profession}</h4>
        </div>
    );
}

function Friend(props) {
    console.log(props);
    return (
        <div className="person">
            <h2>Name: {props.name}</h2>
            <h5>Profession: {props.profession}</h5>
        </div>
    );
}

function Friend2(props) {
    console.log(props);
    return (
        <div className={'person'}>
            <h3>Phone: {props.phone}</h3>
            <h5>Address: {props.address}</h5>
        </div>
    );
}

export default App;
