import logo from './logo.svg';
import './App.css';

function App() {
    const fruits = ['Apple', 'Orange', 'Papaya', 'Lychee', 'Pineapple', 'Strawberry'];

    const employees = [
        {name: 'Ashraful Islam', designation: 'Web-Developer', language: 'Javascript'},
        {name: 'Akhi', designation: 'Front-End Web-Developer', language: 'React'},
        {name: 'Aziz', designation: 'Software Engineer', language: 'Java'}
    ]
    return (
        <div className="App">
            <ul>
                {fruits.map((fruit, index) => <li key={index}>{fruit}</li>)}

            </ul>

            {
                employees.map((employee,index) => <Employee key={index}
                    name={employee.name} designation={employee.designation} language = {employee.language}></Employee>)
            }

        </div>
    );
}

// Components

function Employee(props) {
    return (
        <div className="person">
            <h2>Name: {props.name}</h2>
            <h3>Designation: {props.designation}</h3>
            <h4>Language: {props.language}</h4>
        </div>
    );
}

export default App;
