import React from 'react';
import {addToDb} from "../../utilities/fakedb";

const Cosmetic = (props) => {
    // console.log(props.cosmetic);
    const {name, gender, email, phone, _id} = props.cosmetic;
    const handlePurchase = id => {
        // set to local storage
        console.log(id);
        addToDb(id);
    }
    return (
        <div>
            <h2>Name: {name}</h2>
            <h3>Gender: {gender}</h3>
            <p>_id: {_id} Email: {email}</p>
            <p>Phone: {phone}</p>
            <button onClick={() => handlePurchase(_id)}>Purchase</button>
        </div>
    );
};

export default Cosmetic;
