import React from 'react';
import {add, multiply} from "../../utilities/storage";

const Cosmetics = () => {
    const first = 10;
    const second = 9;
    const sum = add(first, second);
    const multi = multiply(first, second);
    return (
        <div>
            <h3 style={{color:'purple'}}>Multiplication of {first} x {second} = {multi}</h3>
            <h4 style={{color:'darkcyan'}}>Sum of {first} + {second} = {sum}</h4>
        </div>
    );
};

export default Cosmetics;
