// Reducer
const numbs = [34, 52, 25, 47, 89, 30];
const reducer = (previous, current) => previous + current;
numbs.reduce(reducer, 0);

const products = [
    {id: 1, name: 'Camera', price: 55000},
    {id: 2, name: 'Mobile', price: 25000},
    {id: 3, name: 'Laptop', price: 155000},
    {id: 4, name: 'Watch', price: 3000},
]

// Normal Way
let total = 0;
for (const product of products) {
    total += product.price;
}

// Using Reducer
const productReducer = (previous, current) => previous + current.price;
const productTotal = products.reduce(productReducer, 0);