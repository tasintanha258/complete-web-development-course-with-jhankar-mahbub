import React from 'react';

const Cosmetic = (props) => {
    console.log(props.cosmetic);
    const {name, gender, email, phone} = props.cosmetic;
    return (
        <div>
            <h2>{name}</h2>
            <h3>{gender}</h3>
            <p>{email}</p>
            <p>{phone}</p>
        </div>
    );
};

export default Cosmetic;
