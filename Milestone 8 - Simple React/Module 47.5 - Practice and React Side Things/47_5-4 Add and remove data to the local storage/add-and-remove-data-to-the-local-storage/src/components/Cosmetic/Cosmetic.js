import React from 'react';
import {addToDb, deleteFromDb} from "../../utilities/fakedb";

const Cosmetic = (props) => {
    // console.log(props.cosmetic);
    const {name, gender, email, phone, _id} = props.cosmetic;
    const handlePurchase = id => {
        // set to local storage
        console.log(id);
        addToDb(id);
    }

    const handleRemove = id => {
        deleteFromDb(id);
    };

    return (
        <div>
            <h2>Name: {name}</h2>
            <h3>Gender: {gender}</h3>
            <p>_id: {_id} Email: {email}</p>
            <p>Phone: {phone}</p>
            <button onClick={() => handlePurchase(_id)}>Purchase</button>
            <button onClick={() => handleRemove(_id)}>Remove</button>
        </div>
    );
};

export default Cosmetic;
