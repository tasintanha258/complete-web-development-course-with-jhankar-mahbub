import React from 'react';
import './Product.css';

const Product = (props) => {
    console.log(props.product);
    const {img, name, price, seller, stock, key} = props.product;
    return (
        <div className='product'>
            <div>
                <img src={img} alt=""/>
            </div>
            <div>
                <h4 className='product-name'>{name}</h4>
                <p><small>by: {seller}</small></p>
                <p>Price: {price}</p>
                <p><small>Only {stock} products left</small></p>
            </div>
        </div>

    );
};

export default Product;
